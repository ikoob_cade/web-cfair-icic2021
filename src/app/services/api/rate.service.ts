import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RateService {
  private serverUrl = '/events/:eventId/rates';

  constructor(
    private http: HttpClient,
  ) { }

  findOne(agendaId: string, params): Observable<any> {
    return this.http.get(this.serverUrl + '/' + agendaId, { params })
      .pipe(catchError(this.handleError));
  }

  create(body: any): Observable<any> {
    return this.http.post(this.serverUrl, body)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }

    // Return an observable with a user-facing error message.
    return throwError('Something bad happened; please try again later.');
  }

}
