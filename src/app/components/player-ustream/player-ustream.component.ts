import { Component, OnInit, AfterViewInit, ChangeDetectorRef, HostListener, Input, ViewChild, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { _ParseAST } from '@angular/compiler';
// import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import '@devmobiliza/videojs-vimeo/dist/videojs-vimeo.esm';
import { MemberService } from '../../services/api/member.service';
import * as _ from 'lodash';
import { DomSanitizer } from '@angular/platform-browser';

declare let UstreamEmbed: any;

/**
 * TODO
 * Ustream 라이브시 이 컴포넌트를 사용한다.
 * 삼성서울병원 전용 air에서 개발 되어서. Room이 하나밖에 없었기 때문에 live url이 하드코딩 되어있다.
 * 각 룸에 liveUrl을 등록 해서 url 추적하는 방식으로 개발 필요하다.
 */
@Component({
  selector: 'app-player-ustream',
  templateUrl: './player-ustream.component.html',
  styleUrls: ['./player-ustream.component.scss']
})
export class PlayerUstreamComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input('content') content: any; // 전달받은 콘텐츠 정보
  @Output('checkIsLastSession') checkIsLastSession = new EventEmitter(); // 마지막세션 확인

  public user: any;
  public viewer: any;
  public liveUrl;

  @ViewChild('commentList') commentList: any; // 댓글 목록

  public replyForm: FormGroup;
  public replys: any = [];
  private relationId: string;
  private timerID;
  @Input('selected') selected: any; // 전달받은 날짜/룸(채널) 정보


  // TODO 임시데이타
  constructor(
    public fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private memberService: MemberService,
    private sanitizer: DomSanitizer
  ) {
    this.replyForm = fb.group({
      content: ['', Validators.compose([Validators.required, Validators.minLength(10)])]
    });
  }

  ngOnInit(): void {
    this.liveUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.content.contentUrl);
    this.user = JSON.parse(sessionStorage.getItem('cfair'));
    this.relationId = this.selected.room.id;
    this.getComment();
    this.timerID = setInterval(() => {
      this.getComment();
    }, 10 * 1000);
  }

  ngOnDestroy(): void {
    clearInterval(this.timerID);
  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges();
    this.setUstreamViewer();
  }

  setUstreamViewer(): void {
    if (UstreamEmbed) {
      this.viewer = UstreamEmbed('UstreamIframe');
      // document.getElementById('UstreamIframe').style.minHeight = '-webkit-fill-available';

      this.viewer.addListener('live', (event, data) => {
        console.log(event + ' = ' + data);
      });

      this.viewer.addListener('finished', (event, data) => {
        console.log(event + ' = ' + data);
        this.viewer.getProperty('progress', function (progress) {
          console.log('progress = ' + progress);
        });
      });
      this.viewer.addListener('offline', (event, data) => {
        console.log(event + ' = ' + data);
        this.viewer.getProperty('progress', function (progress) {
          console.log('progress = ' + progress);
        });
      });
      this.viewer.addListener('playing', (event, data) => {
        console.log(event + ' = ' + data);
      });
    } else {
      console.log('no UstreamEmbed');
    }
  }

  /** 댓글달기 */
  comment(): void {
    this.memberService.createComment({
      memberId: this.user.id,
      title: '',
      content: this.replyForm.value.content,
      relationId: this.relationId
    }).subscribe(res => {
      this.getComment();
      // this.replyForm.value.content = '';
      this.replyForm.patchValue({
        content: ''
      });

      // this.downScroll();
    });
  }

  /** 채팅창을 최하단으로 스크롤한다. */
  downScroll(): void {
    this.commentList.nativeElement.scrollTop = this.commentList.nativeElement.scrollHeight;
  }

  /** 본인이 입력한 채팅 여부 (삭제 출력) */
  check(comment): boolean {
    if (comment && comment.memberId === this.user.id) {
      return true;
    }
    return false;
  }

  /** 채팅을 삭제한다. */
  delete(comment): void {
    if (confirm('Are you sure you want to delete?')) {
      this.memberService.deleteComment(this.user.id, comment.id)
        .subscribe(res => {
          this.getComment();
        });
    }
  }

  /** 댓글 불러오기 */
  getComment(): void {
    this.memberService.findComment(this.user.id, this.relationId).subscribe(res => {
      if (res) {
        if (res.length === 1 && !res[0].id) {
          this.replys = [];
        } else {
          this.replys = res;
        }
        
        this.checkIsLastSession.emit(res[0].now);
      }
    });
  }
}
