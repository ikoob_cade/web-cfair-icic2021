import * as _ from 'lodash';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { MemberService } from '../../services/api/member.service';
import { SocketService } from '../../services/socket/socket.service';
import { FunctionService } from '../../services/function/function.service';

declare var $: any;
enum Change_Password_Message {
  FILL_ALL = '모든 항목을 입력해 주세요.',
  FAILED_NEW_CONFIRM = '새 비밀번호 확인이 잘못 입력되었습니다.\n다시 확인해 주세요.',
  SUCCESS = 'Password Changed Successfully',
  CANNOT_BLANK = "Your password can't start or end with a blank space"
}

@Component({
  selector: 'app-my-page',
  templateUrl: './my-page.component.html',
  styleUrls: ['./my-page.component.scss']
})
export class MyPageComponent implements OnInit, AfterViewInit {
  public user: any;
  public histories: Array<any>;
  public historiesOfVod: Array<any>;
  public boothsOfStamp: Array<any>[];
  public totalTime = null;
  public booths: [];
  public PASSWORD_OLD = '';
  public PASSWORD_NEW = '';
  public PASSWORD_CONFIRM = '';

  public hour;
  public hourOfVod;
  public min;
  public minOfVod;
  public secOfVod;

  public selectedHistory;

  constructor(
    public router: Router,
    private authService: AuthService,
    private memberService: MemberService,
    private socketService: SocketService,
    private functionService: FunctionService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse(sessionStorage.getItem('cfair'));

    if (!this.user) {
      this.router.navigate(['/main']);
    }

    this.getMyInfo();
    this.getMyHistory();
    // this.getMyHistoryOfVod();
    // this.getVisitBooth();
  }

  ngAfterViewInit(): void {
    this.checkIsAutoExit();
  }

  /** 사용자 정보 조회 */
  getMyInfo(): void {
    this.memberService.getMyInfo(this.user.id).subscribe(res => {
      if (res) {
        this.user = res;
      }
    });
  }

  /*
   * 사용자 시청기록 조회
   * live 기록의 total은 분 단위
   */
  getMyHistory(): void {
    this.memberService.getMyHistoryOfSession(this.user.id).subscribe(res => {
      if (res && res.logs) {
        if (res.totalTime < 60) {
          this.hour = '00';
        } else {
          this.hour = Math.floor(res.totalTime / 60).toString();
          if (this.hour.length < 2) {
            this.hour = '0' + this.hour;
          }
        }

        this.min = (res.totalTime % 60).toString();
        if (this.min.length < 2) {
          this.min = '0' + this.min;
        }
        this.histories = this.refineLogs(res.logs);
      }
    });
  }

  /*
   * 사용자 VOD 시청기록 조회
   * vod기록 total은 초단위로 받는다.
   */
  getMyHistoryOfVod(): void {
    this.memberService
      .getMyHistoryOfVod(
        this.user.id,
        '2021-10-06 15:00:00', '2021-10-09 15:00:00')
      .subscribe(res => {
        if (res) {
          this.historiesOfVod = res.logs;

          const total = res.total;
          // let total = 3757; // test : 1시간 2분 37초
          if (total < 3600) {
            this.hourOfVod = '00';
          } else {
            this.hourOfVod = Math.floor(total / 3600).toString();
            if (this.hourOfVod.length < 2) {
              this.hourOfVod = '0' + this.hourOfVod;
            }
          }

          this.minOfVod = Math.floor(total % 3600 / 60).toString();

          if (this.minOfVod.length < 2) {
            this.minOfVod = '0' + this.minOfVod;
          }

          this.secOfVod = Math.floor(total % 3600 % 60).toString();
          if (this.secOfVod.length < 2) {
            this.secOfVod = '0' + this.secOfVod;
          }

          // watchTime to mm:ss
          for (const vod of this.historiesOfVod) {
            if (vod.watchTime) {
              const watchTime = new Date(0);
              watchTime.setHours(0);
              watchTime.setSeconds(vod.watchTime);
              vod.watchTime = watchTime;
              // vod.watchTime = new Date(0).setSeconds(vod.watchTime).toString();
            } else {
              vod.watchTime = '00:00';
            }
          }
        }
      });
  }

  checkIsAutoExit(): void {
    setTimeout(() => {
      this.activatedRoute.queryParams.subscribe(params => {
        if (params.isAutoExit === 'true') {
          alert('금일 행사가 종료되었습니다.\n로그아웃 해주시기 바랍니다.');
        }
      });
    }, 500);
  }

  refineLogs(logsOfDates): Array<any> {
    const histories = [];
    // tslint:disable-next-line: forin
    for (const i in logsOfDates) {
      let hour;
      let min;

      logsOfDates[i].datas = _.groupBy(logsOfDates[i].datas, 'roomName');

      if (logsOfDates[i].totalTime < 60) {
        hour = '00';
      } else {
        hour = Math.floor(logsOfDates[i].totalTime / 60).toString();
        if (hour.length < 2) {
          hour = '0' + hour;
        }
      }

      min = (logsOfDates[i].totalTime % 60).toString();
      if (min.length < 2) {
        min = '0' + min;
      }
      histories.push({ date: i, rooms: logsOfDates[i].datas, hour, min });
    }
    return histories;
  }

  /** 로그아웃 */
  logout(): void {
    this.authService.logout().subscribe(res => {
      this.logoutSocket();

      sessionStorage.removeItem('cfair');
      this.router.navigate(['/']);
    }, error => {
      sessionStorage.removeItem('cfair');
      this.router.navigate(['/']);
    });
  }

  logoutSocket(): void {
    this.socketService.logout({
      memberId: this.user.id
    });
  }

  /** 비밀번호 변경 */
  changePassword(): void {
    const errorMessage = this.passwordValidator();
    if (!errorMessage) {
      const body = {
        password: this.PASSWORD_OLD,
        newPassword: this.PASSWORD_NEW
      };

      this.memberService.changePassword(body).subscribe(res => {
        alert('Your password has been changed.');
        this.cancelChangePwd();
      }, error => {
        if (error.error.statusCode === 401) {
          alert('현재 비밀번호가 틀렸습니다.');
        }
      });
    } else {
      alert(errorMessage);
    }
  }

  /** 비밀번호 변경 취소 */
  cancelChangePwd(): void {
    $('#changePassword').modal('hide');
    this.PASSWORD_NEW = '';
    this.PASSWORD_CONFIRM = '';
  }

  /** 비밀번호 변경 유효성 검사 */
  passwordValidator(): string {
    const conditions =
      [
        this.PASSWORD_NEW.length >= 1 && this.PASSWORD_NEW[0] === ' ',
        this.PASSWORD_CONFIRM.length >= 1 && this.PASSWORD_CONFIRM[0] === ' '
      ];
    if (conditions[0] || conditions[1]) {
      return Change_Password_Message.CANNOT_BLANK;
    }

    if (!this.PASSWORD_OLD || !this.PASSWORD_NEW || !this.PASSWORD_CONFIRM) {
      return Change_Password_Message.FILL_ALL;
    } else if (this.PASSWORD_NEW !== this.PASSWORD_CONFIRM) {
      return Change_Password_Message.FAILED_NEW_CONFIRM;
    }
    return null;
  }

  /*
   * 부스 조회 (나의 부스방문 기록)
   */
  private getVisitBooth(): void {
    if (this.user) {
      const customCondition = {
        isStampReset: true,
        isStampGrade: true,
      };

      this.memberService.findVisitors(this.user.id, customCondition)
        .subscribe((data: any) => {
          this.boothsOfStamp = this.functionService.division(data.slice(0, 15), 5);
        });
    }
  }
}
