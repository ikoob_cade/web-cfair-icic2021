export const environment = {
  production: true,
  base_url: 'https://d3v-server-cf-air-api.ikoob.co.kr/api/v1',
  socket_url: 'https://d3v-server-cf-air-socket-api.ikoob.co.kr',
  eventId: '617b601d007e790013debde7' // ICIC2021 DEV
};
